<?php

use Illuminate\Database\Seeder;

class OrgUsersSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('org_users')->insert([
            ['login' => 'admin', 'password' =>Hash::make('123456')]
        ]);
    }
}
