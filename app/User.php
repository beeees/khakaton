<?php

namespace App;


use Illuminate\Database\Eloquent\Builder;

class User extends \App\Models\User
{
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('user', function (Builder $builder) {
            $builder->where('role', '=', self::USER_ROLE);
        });
    }
}