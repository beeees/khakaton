<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/home';


    public function __construct()
    {
        $this->middleware('guest')->only(['loginWithRole', 'login']);
        $this->middleware('auth')->only(['logout']);
    }

    protected function guard()
    {
        $guard = 'web';
        if (request('role') == User::EMPLOYER_ROLE) {
            $guard = 'employer-web';
        }
        return Auth::guard($guard);
    }

    public function loginWithRole(LoginRequest $request) {
        return $this->login($request);
    }

}
