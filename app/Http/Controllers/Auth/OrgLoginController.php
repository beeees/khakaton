<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Request;

class OrgLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/org/home';


    public function __construct()
    {
        $this->middleware('guest')->only(['loginWithRole', 'login']);
        $this->middleware('auth:org')->only(['logout']);
    }

    protected function guard()
    {
        return Auth::guard('org-web');
    }

    public function showLoginForm()
    {
        return view('auth.login-org');
    }

    public function username()
    {
        return 'login';
    }


}
