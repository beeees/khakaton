<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Arr;
use Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth:web,employer-web')->except('assist');
    }

    public function main()
    {
        return view('home');
    }

    public function assist(Request $request)
    {
        Storage::disk('public')->put('info-debug.txt', json_encode($request->all()));
        echo Storage::disk('public')->url('info-debug.txt');
        return ;
    }
}
