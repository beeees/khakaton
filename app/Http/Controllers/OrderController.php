<?php

namespace App\Http\Controllers;

use App\Http\Requests\AppRequest;
use App\Http\Requests\OrderCreateRequest;
use App\Http\Requests\OrderListRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Models\Order;
use Illuminate\Auth\AuthenticationException;

class OrderController extends Controller
{
    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:employer-web,org-web')->except('index', 'show');
        $this->middleware('auth')->only('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @param OrderListRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(OrderListRequest $request)
    {
        $where = $request->all();
        if ($user = $request->user('web')) {
            $where['is_visile'] = true;
        }elseif($user = $request->user('employer-web')) {
            $where['user_id'] = $user->id;
        }
        return view('order.list', [
            'results' => Order::where($where)->paginate('20'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AppRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderCreateRequest $request)
    {
        $order = Order::create($request->only('name', 'description'));
        $order->is_visible = false;
        if ($user = request()->user('employer-web') || $user = $request->user('org-web')) {
            $order->user()->save($user);
        }
        $order->status = Order::STATUS_SEARCH_WORKER;
        $order->is_org_order =  request()->user('employer-web') ? false : true;
        $order->save();
        return view('order.show', compact('order'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        return view('order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        return view('order.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderUpdateRequest $request, $id)
    {
        ($order = Order::findOrFail($id))->fill($request->all());
        if (($emp = $request->user('employer-web')) && $emp->id !== $order->user->id){
            throw new AuthenticationException('Not permission!');
        }
        if ($org = $request->user('org-web')) {
            if ($request->is_visible && !$order->is_visible) {
                $order->is_visible = true;
                $order->status = Order::STATUS_SEARCH_WORKER;
            }
        }
        $order->save();
        return view('order.show', compact('order'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::findOrFail($id)->delete();
        return view('order.index');
    }
}
