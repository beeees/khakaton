<?php

namespace App\Http\Requests\Auth;


use App\Http\Requests\AppRequest;

class SignupRequest extends AppRequest
{
    public const NAME_RULE = 'string|min:2|max:30';
    public const NAME_RULE_REQUIRED = 'required|' . self::NAME_RULE;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => self::NAME_RULE_REQUIRED,
            'email' => self::EMAIL_RULE_REQUIRED . '|unique:users,email',
            'password' => self::PASSWORD_RULE_REQUIRED . '|confirmed',
            'password_confirmation' => [],
            'role' => 'required|integer|in:0,1',
        ];
    }
}
