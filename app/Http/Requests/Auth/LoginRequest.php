<?php

namespace App\Http\Requests\Auth;


use App\Http\Requests\AppRequest;

class LoginRequest extends AppRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => self::EMAIL_RULE_REQUIRED,
            'password' => self::PASSWORD_RULE_REQUIRED,
        ];
    }
}
