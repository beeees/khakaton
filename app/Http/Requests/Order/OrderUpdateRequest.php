<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class OrderUpdateRequest extends OrderCreateRequest
{
    public function rules()
    {
        return [
            'name' => self::ORDER_NAME,
            'description' => self::ORDER_DESCRIPTION,
            'is_visible' => self::BOOLEAN,
            'status' => self::ORDER_STATUS,
        ];
    }
}
