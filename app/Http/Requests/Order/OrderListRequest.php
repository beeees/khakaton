<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class OrderListRequest extends AppRequest
{
    public function rules()
    {
        return [
            'status' => self::ORDER_STATUS,
        ];
    }
}
