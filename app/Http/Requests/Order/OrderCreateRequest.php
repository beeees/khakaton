<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class OrderCreateRequest extends AppRequest
{
    public function rules()
    {
        return [
            'name' => self::ORDER_NAME_REQUIRED,
            'description' => self::ORDER_DESCRIPTION_REQUIRED,
        ];
    }
}
