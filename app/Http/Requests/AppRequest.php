<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class AppRequest extends FormRequest
{
    public const PASSWORD_RULE = 'string|min:6|max:255';
    public const PASSWORD_RULE_REQUIRED = 'required|' . self::PASSWORD_RULE;
    public const EMAIL_RULE = 'email';
    public const EMAIL_RULE_REQUIRED = 'required|email';
    public const BOOLEAN = 'boolean';
    public const ORDER_NAME = 'string|min:10';
    public const ORDER_DESCRIPTION = 'string|min:20';
    public const ORDER_NAME_REQUIRED = 'required|' . self::ORDER_NAME;
    public const ORDER_DESCRIPTION_REQUIRED = 'required|' . self::ORDER_DESCRIPTION;
    public const ORDER_STATUS = 'integer';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }

    public function all($keys = null)
    {
        return parent::all(array_keys(static::rules()));
    }

    public function rules()
    {
        return [];
    }
}
