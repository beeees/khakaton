<?php

namespace App;


use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class Employer extends User
{
    protected $table  = 'users';

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('employer', function (Builder $builder) {
            $builder->where('role', '=', self::EMPLOYER_ROLE);
        });
    }
}