<?php

namespace App\Models;

use App\Employer;
use App\Org;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Order
 * @package App\Models
 * @property $name
 * @property $description
 * @property $is_org_order
 * @property $status
 * @property $is_visible
 * @property $user_id
 * @property $worker_id
 * @property Employer|Org $user
 * @property User $worker
 */
class Order extends Model
{
    public const STATUS_SEARCH_WORKER = 1;
    public const STATUS_IN_WORK = 2;
    public const STATUS_CLOSED = 3;
    protected $fillable = [
        'name',
        'description',
    ];
    protected $hidden = [
        'user',
        'user_id',
    ];

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function worker() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
