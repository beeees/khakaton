<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App\Models
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    public const USER_ROLE = 0;
    public const EMPLOYER_ROLE = 1;
    public const ADMIN_ROLE = 2;
    public const ROLES = [
        'Подрядчик' => self::USER_ROLE,
        'Заказчик' => self::EMPLOYER_ROLE,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'token',
    ];
}
