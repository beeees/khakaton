<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@main')->name('root');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/org/login', 'Auth\OrgLoginController@showLoginForm')->name('org-login');
Route::post('/login', 'Auth\LoginController@loginWithRole');
Route::post('/org/login', 'Auth\OrgLoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/signup', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/signup', 'Auth\RegisterController@register');
Route::get('/home', 'Controller@main');
Route::get('/org/home', 'OrgController@main');
Route::resource('orders', 'OrderController');
Route::post('/assist', 'Controller@assist');
Route::get('/assist', 'Controller@assist');
